package se.experis;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class ProgramTest {
    private Program checkCreditCard;
    @BeforeEach
    void beforeEach() {
        checkCreditCard = new Program();
    }

    @Test
    void validCreditCard() {
        assertEquals(8, checkCreditCard.checkLuhn("4234834642529848"));
        assertNotEquals(3, checkCreditCard.checkLuhn("4234834642529848"));
    }

    @Test
    void checkSum() {
        assertEquals("Valid", checkCreditCard.checkSum(1, 1));
        assertNotEquals("Valid", checkCreditCard.checkSum(1, 3));
        assertNull(null, checkCreditCard.checkSum(1, 3));
    }

    @Test
    void checkTimeOut() {
        assertTimeoutPreemptively(Duration.ofSeconds(1), () -> checkCreditCard.checkLuhn("4234834642529848"));
        assertTimeoutPreemptively(Duration.ofSeconds(1), () -> checkCreditCard.checkInput("4234834642529848"));

    }

    @Test
    void checkInput() {
        assertEquals("423483464252984 8", checkCreditCard.checkInput("4234834642529848"));
        assertNotEquals("4234834642529848", checkCreditCard.checkInput("4234834642529848"));
    }

    @Test
    void checkForInvalidChars() {
        assertEquals(null, checkCreditCard.checkInput("Hello"));
    }
}