package se.experis;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        System.out.println("Enter credit card:");
        Scanner scanner = new Scanner(System.in);
        String creditCard = scanner.nextLine();


        int expected = checkLuhn(creditCard);
        System.out.println("Input: " + checkInput(creditCard));
        int provided;
        if (checkInput(creditCard) != null) {
            provided = Integer.parseInt(creditCard.substring(creditCard.length() - 1 , creditCard.length()));
        } else {
            provided = -1;
        }
        System.out.println("Provided: " + (provided != -1 ? provided : "You're only allowed tp type numbers"));
        System.out.println("Expected: " + expected);
        System.out.println("----------------");
        System.out.println("checkSum: " + (checkSum(provided, expected) ==  "Valid" ? "Valid" : "Invalid"));
        System.out.println("Digits: " + creditCard.length());
    }

    // Checks that provided and and expected number are the same
    public static String checkSum(int providedNumber, int expectedNumber) {
        if (providedNumber == expectedNumber) {
            return "Valid";
        } else {
            return null;
        }
    }

    // Regex that checks if only numbers is applied from user
    public static String checkInput(String creditCard) {
        StringBuilder stringBuilder = new StringBuilder(creditCard);
        int creditCardLength = creditCard.length();
        stringBuilder.insert(creditCardLength - 1, " ");

        String regex = "[0-9]+";
        if (creditCard.matches(regex)) {
            return stringBuilder.toString();
        } else {
            return null;
        }

    }

    // Luhn algoritm that returns the expected check number
    public static int checkLuhn(String cardNo) {

        int nDigits = cardNo.length();
        int nSum = 0;
        boolean isSecond = true;
        boolean isFirstChar = true;
        String removedLastChar = cardNo.replace(".$","");

        for (int i = nDigits - 1; i >= 0; i--) {

            int currentDigit = removedLastChar.charAt(i) - '0';

            if (isFirstChar){
                isFirstChar = false;
                continue;
            }

            if (isSecond) {

                if ((currentDigit * 2) > 9) {
                    nSum += (currentDigit * 2) - 9;
                } else {
                    nSum += currentDigit * 2;
                }

            } else {
                nSum += currentDigit;
            }

            isSecond = !isSecond;
        }

        nSum = nSum * 9;
        return (nSum % 10);
    }
}
